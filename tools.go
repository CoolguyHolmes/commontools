package commontools

import (
	"bufio"
	"fmt"
	"os"
)

func (f *Fd) SelfDefinedWriteString(sentence string) (int, error) {
	i, e := f.Writer.WriteString(sentence)
	if e != nil {
		return i, e
	}
	return i, f.Flush()
}

func (f *Fd) SelfDefinedReadString(delim byte) (string, error) {
	return f.Reader.ReadString(delim)
}

func (f *Fd) Handler() {
	f.Close()
	return
}

func FdInit(fileName string) *Fd {
	file, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		panic(fmt.Sprintf("文件打开失败, err: %v", err))
	}
	write := bufio.NewWriter(file)
	reader := bufio.NewReader(file)
	return &Fd{
		File:   file,
		Writer: write,
		Reader: reader,
	}
}

type Fd struct {
	*os.File
	*bufio.Writer
	*bufio.Reader
}
